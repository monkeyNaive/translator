(in-package :asdf-user)

(defsystem "translator"
  :version "0.1.0"
  :author "leejoker"
  :license "MIT"
  :depends-on ("dexador" "local-time" "ironclad" "cl-ppcre" "com.inuoe.jzon" "babel")
  :components ((:module "src"
                :components
                ((:file "main")
		 (:file "config"))))
  :build-operation "program-op" ;; leave as is
  :build-pathname "translator"
  :entry-point "translator:main"
  :description ""
  :in-order-to ((test-op (test-op "translator/tests"))))

(defsystem "translator/tests"
  :author ""
  :license ""
  :depends-on ("translator"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for translator"
  :perform (test-op (op c) (symbol-call :rove :run c)))
