(defpackage translator/tests/main
  (:use :cl
        :translator
        :rove))
(in-package :translator/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :translator)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
